﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Calculator.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/value01/value02/symbol
        [HttpGet("{value01}/{value02}/{symbol}")]
        public ActionResult<string> Get(double value01, double value02, string symbol)
        {
            double total = 0;
            switch (symbol)
            {
                case "ADD":
                    total = value01 + value02;
                    break;
                case "SUBTRACT":
                    total = value01 - value02;
                    break;
                case "MULTIPLY":
                    total = value01 * value02;
                    break;
                case "DIVIDE":
                    if (value02 == 0)
                    {
                        return "Can't devide by zero!";
                    }
                    total = value01 / value02;
                    break;
            }
            return total.ToString();
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
