import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectCalculator = state => state.get('calculator', initialState);

const makeSelectLoading = () =>
  createSelector(selectCalculator, globalState => globalState.get('loading'));
const makeSelectError = () =>
  createSelector(selectCalculator, globalState => globalState.get('error'));
const makeSelectNum01 = () =>
  createSelector(selectCalculator, globalState => globalState.get('num01'));
const makeSelectNum02 = () =>
  createSelector(selectCalculator, globalState => globalState.get('num02'));
const makeSelectSymbol = () =>
  createSelector(selectCalculator, globalState => globalState.get('symbol'));
const makeSelectTotal = () =>
  createSelector(selectCalculator, globalState => globalState.get('total'));
const makeSelectRecords = () =>
  createSelector(selectCalculator, globalState => globalState.get('records'));

export {
  selectCalculator,
  makeSelectLoading,
  makeSelectError,
  makeSelectNum01,
  makeSelectNum02,
  makeSelectSymbol,
  makeSelectTotal,
  makeSelectRecords,
};
