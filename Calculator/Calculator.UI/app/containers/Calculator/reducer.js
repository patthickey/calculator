import { fromJS, Record, List } from 'immutable';

import {
  SETVALUE,
  STARTMATH,
  STARTMATHSUCCESS,
  STARTMATHERROR,
  RESETRECORDS,
} from './constants';

export const Data = new Record({
  num01: '',
  num02: '',
  symbol: '',
  total: '',
});

// The initial state of the App
export const initialState = fromJS({
  loading: false,
  error: null,
  num01: '',
  num02: '',
  symbol: '',
  records: List([]),
});

function calculatorReducer(state = initialState, action) {
  switch (action.type) {
    case SETVALUE:
      return state.set(action.detail, action.value);
    case STARTMATH:
      return state.set('loading', true);
    case STARTMATHSUCCESS:
      return state
        .set('loading', false)
        .set('num01', '')
        .set('num02', '')
        .set('symbol', '')
        .update('records', data => data.push(action.record));
    case STARTMATHERROR:
      return state.set('loading', false).set('error', action.error);
    case RESETRECORDS:
      return state.set('records', List([]));
    default:
      return state;
  }
}

export default calculatorReducer;
