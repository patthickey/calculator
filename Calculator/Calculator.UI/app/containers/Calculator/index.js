import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';

import {
  TextField,
  InputLabel,
  Button,
  Chip,
  MenuItem,
} from '@material-ui/core';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import reducer from './reducer';
import saga from './saga';
import {
  makeSelectLoading,
  makeSelectError,
  makeSelectNum01,
  makeSelectNum02,
  makeSelectSymbol,
  // makeSelectTotal,
  makeSelectRecords,
} from './selectors';
import { setValue, startMath, resetRecords } from './actions';

const symbols = [
  {
    value: 'ADD',
    label: '+',
  },
  {
    value: 'SUBTRACT',
    label: '-',
  },
  {
    value: 'MULTIPLY',
    label: '*',
  },
  {
    value: 'DIVIDE',
    label: '/',
  },
];

/* eslint-disable react/prefer-stateless-function */
export class Calculator extends React.PureComponent {
  render() {
    return (
      <div>
        <form onSubmit={this.props.onSubmit}>
          <InputLabel htmlFor="num01">
            1st Number:
            <TextField
              name="num01"
              type="number"
              value={this.props.num01}
              onChange={this.props.onChange}
            />
          </InputLabel>
          <br />
          <InputLabel htmlFor="symbol">
            Symbol:
            <TextField
              select
              name="symbol"
              value={this.props.symbol}
              onChange={this.props.onChange}
            >
              {symbols.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </InputLabel>
          <br />
          <InputLabel htmlFor="num02">
            2nd Number:
            <TextField
              name="num02"
              type="number"
              value={this.props.num02}
              onChange={this.props.onChange}
            />
          </InputLabel>
          <br />
          <Button
            type="submit"
            color="primary"
            variant="raised"
            disabled={this.props.loading}
          >
            submit
          </Button>
        </form>
        {this.props.records &&
          this.props.records.map(record => (
            <Chip
              label={`${record.num01} ${record.symbol} ${record.num02} = ${
                record.total
              }`}
            />
          ))}
        {this.props.records && (
          <Button
            type="clear"
            color="primary"
            variant="raised"
            onClick={this.props.onReset}
          >
            Clear
          </Button>
        )}
        {this.props.error}
      </div>
    );
  }
}

Calculator.propTypes = {
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([PropTypes.object, PropTypes.bool]),
  onSubmit: PropTypes.func,
  num01: PropTypes.string,
  num02: PropTypes.string,
  symbol: PropTypes.string,
  // total: PropTypes.string,
  onChange: PropTypes.func,
  records: PropTypes.object,
  onReset: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    onChange: evt => dispatch(setValue(evt.target.name, evt.target.value)),
    onSubmit: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(startMath());
    },
    onReset: evt => {
      if (evt !== undefined && evt.preventDefault) evt.preventDefault();
      dispatch(resetRecords());
    },
  };
}

const mapStateToProps = createStructuredSelector({
  num01: makeSelectNum01(),
  num02: makeSelectNum02(),
  symbol: makeSelectSymbol(),
  records: makeSelectRecords(),
  // total: makeSelectTotal(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'calculator', reducer });
const withSaga = injectSaga({ key: 'calculator', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Calculator);
