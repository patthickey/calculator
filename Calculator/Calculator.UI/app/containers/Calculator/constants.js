export const SETVALUE = 'calculator.ui/Calculator/SETVALUE';
export const STARTMATH = 'calculator.ui/Calculator/STARTMATH';
export const STARTMATHSUCCESS = 'calculator.ui/Calculator/STARTMATHSUCCESS';
export const STARTMATHERROR = 'calculator.ui/Calculator/STARTMATHERROR';
export const RESETRECORDS = 'calculator.ui/Calculator/RESETRECORDS';
