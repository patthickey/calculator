import {
  SETVALUE,
  STARTMATH,
  STARTMATHSUCCESS,
  STARTMATHERROR,
  RESETRECORDS,
} from './constants';

export function setValue(detail, value) {
  return {
    type: SETVALUE,
    detail,
    value,
  };
}

export function startMath() {
  return {
    type: STARTMATH,
  };
}

export function startMathSuccess(record) {
  return {
    type: STARTMATHSUCCESS,
    record,
  };
}

export function startMathError(error) {
  return {
    type: STARTMATHERROR,
    error,
  };
}

export function resetRecords() {
  return {
    type: RESETRECORDS,
  };
}
