import { call, put, select, takeLatest } from 'redux-saga/effects';
import request from 'utils/request';

import { Data } from './reducer';
import { STARTMATH } from './constants';

import {
  makeSelectNum01,
  makeSelectNum02,
  makeSelectSymbol,
} from './selectors';
import { startMathSuccess, startMathError } from './actions';

export function* getMath() {
  const num01 = yield select(makeSelectNum01());
  const num02 = yield select(makeSelectNum02());
  const symbol = yield select(makeSelectSymbol());
  const requestURL = `http://localhost:62819/api/values/${num01}/${num02}/${symbol}`;
  try {
    const total = yield call(request, requestURL);
    const record = new Data({
      num01,
      num02,
      symbol,
      total,
    });
    yield put(startMathSuccess(record));
  } catch (err) {
    yield put(startMathError(err));
  }
}

export default function* githubData() {
  yield takeLatest(STARTMATH, getMath);
}
